﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace YandexSteamPayer {
	internal static class Program {
		private static HttpClient HttpClient;

		private static void Log(string log) {
			Console.WriteLine(log);
			File.AppendAllText("log.txt", log + Environment.NewLine);
		}

		private static async Task Main() {
			string token;
			decimal sum = 0;
			if (File.Exists("log.txt")) {
				File.Delete("log.txt");
			}

			if (File.Exists("settings.txt")) {
				string[] settings = File.ReadAllLines("settings.txt");
				token = settings[0];
				if (!((settings.Length > 1) && decimal.TryParse(settings[1].Replace(',', '.'), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out sum) && (sum > 0))) {
					Console.WriteLine("Sum was not found or it is incorrect, please enter it:");
					while (sum <= 0) {
						decimal.TryParse(Console.ReadLine(), out sum);
					}
				}
			} else {
				Console.WriteLine("Settings file was not found, please enter token:");
				token = Console.ReadLine();
				Console.WriteLine("Please enter sum:");
				while (sum <= 0) {
					decimal.TryParse(Console.ReadLine()?.Replace(',', '.'), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out sum);
				}
			}

			HttpClient = new HttpClient(new HttpClientHandler {
				AllowAutoRedirect = true
			}) {
				DefaultRequestHeaders = {
					Accept = {
						MediaTypeWithQualityHeaderValue.Parse("application/json")
					},
					Authorization = AuthenticationHeaderValue.Parse($"Bearer {token}"),
					UserAgent = {
						ProductInfoHeaderValue.Parse(nameof(YandexSteamPayer) + "/1.0")
					}
				}
			};

			List<string> accounts = null;
			if (File.Exists("accounts.txt")) {
				accounts = File.ReadAllLines("accounts.txt").ToList();
			} else {
				Console.WriteLine("File with accounts was not found, please enter accounts joining them by space:");
				while (accounts == null) {
					accounts = Console.ReadLine()?.Split(' ').ToList();
				}
			}

			bool firstIteration = true;
			foreach (string account in accounts) {
				if (firstIteration) {
					firstIteration = false;
				} else {
					await Task.Delay(1000).ConfigureAwait(false);
				}

				try {
					HttpResponseMessage requestPaymentMessage = await HttpClient.PostAsync("https://money.yandex.ru/api/request-payment",
						new StringContent(
							$"pattern_id=5197&ShopArticleID=35016&ShowCaseID=1010&shn=Steam%20Wallet%20%28%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F%29&customerNumber={account}&sum={sum.ToString(CultureInfo.InvariantCulture)}",
							Encoding.UTF8, "application/x-www-form-urlencoded")).ConfigureAwait(false);
					if (!requestPaymentMessage.IsSuccessStatusCode) {
						Log($"[{account}] Error: request-payment|{requestPaymentMessage.StatusCode}");
						continue;
					}

					string responseRequestPayment = await requestPaymentMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
					if (string.IsNullOrEmpty(responseRequestPayment)) {
						Log($"[{account}] Error: request-payment|content is empty");
						continue;
					}

					dynamic requestPaymentJson;
					try {
						requestPaymentJson = JsonConvert.DeserializeObject(responseRequestPayment);
					} catch (JsonException) {
						Log($"[{account}] Error: request-payment|failed to parse response");
						continue;
					}

					string requestPaymentStatus = (string) requestPaymentJson.status;
					if (string.IsNullOrEmpty(requestPaymentStatus)) {
						Log($"[{account}] Error: request-payment|invalid status");
						continue;
					}

					if (requestPaymentStatus != "success") {
						Log($"[{account}] Error: request-payment|{requestPaymentStatus}|{responseRequestPayment}");
						continue;
					}

					string requestID = (string) requestPaymentJson.request_id;
					HttpResponseMessage processPaymentMessage = await HttpClient.PostAsync("https://money.yandex.ru/api/process-payment",
						new StringContent($"request_id={requestID}&money_source=wallet", Encoding.UTF8, "application/x-www-form-urlencoded")).ConfigureAwait(false);
					if (!processPaymentMessage.IsSuccessStatusCode) {
						Log($"[{account}] Error: process-payment|{processPaymentMessage.StatusCode}");
						continue;
					}

					string responseProcessPayment = await processPaymentMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
					if (string.IsNullOrEmpty(responseProcessPayment)) {
						Log($"[{account}] Error: process-payment|content is empty");
						continue;
					}

					dynamic processPaymentJson;
					try {
						processPaymentJson = JsonConvert.DeserializeObject(responseProcessPayment);
					} catch (JsonException) {
						Log($"[{account}] Error: process-payment|failed to parse response");
						continue;
					}

					string processPaymentStatus = (string) processPaymentJson.status;
					if (string.IsNullOrEmpty(processPaymentStatus)) {
						Log($"[{account}] Error: process-payment|invalid status");
						continue;
					}

					if (processPaymentStatus != "success") {
						Log($"[{account}] Error: process-payment|{processPaymentStatus}|{responseProcessPayment}");
						continue;
					}

					Log($"[{account}] Done!");
				} catch (Exception ex) {
					Log($"[{account}] Error: " + ex.Message);
				}
			}

			Console.WriteLine("Press any key to close...");
			Console.ReadKey();
		}
	}
}
